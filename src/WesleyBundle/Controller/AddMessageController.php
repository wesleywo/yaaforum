<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WesleyBundle\Entity\Board;
use WesleyBundle\Entity\Message;
use WesleyBundle\Entity\Post;
use WesleyBundle\Form\AddMessageType;
use WesleyBundle\Form\AddPostType;

class AddMessageController extends Controller
{
    /**
     * @Route("/add-message/{postId}/")
     *
     * @param         $postId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $postId)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('WesleyBundle:Post')->find($postId);
        $user = $this->getUser();
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('danger', 'Log in om een bericht achter te laten.');
            return $this->redirectToRoute('wesley_post_view', ['id' => $postId, 'title' => $post->getTitle()]);
        }
        if (is_null($post)) {
            return $this->createNotFoundException('De opgevraagde aanvraag kan niet worden gevonden');
        }
        $message = new Message();
        $form = $this->createForm(AddMessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $message->setPost($post);
            $message->setUser($user);
            $em->persist($message);
            $em->flush();
            $this->addFlash('success', 'Succesvol gepost.');
            return $this->redirectToRoute('wesley_post_view', ['id' => $postId, 'title' => $post->getTitle()]);
        }

        return $this->render('@Wesley/client/add_message.html.twig', [
            'form' => $form->createView(),
            'post' => $post->getTitle(),
        ]);
    }
}
