<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WesleyBundle\Entity\Board;
use WesleyBundle\Entity\Post;
use WesleyBundle\Entity\User;
use WesleyBundle\Form\AddPostType;
use WesleyBundle\WesleyBundle;

class AddPostController extends Controller
{
    /**
     * @Route("/add-post/{boardId}/")
     *
     * @param         $boardId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $boardId)
    {
        $em = $this->getDoctrine()->getManager();
        $board = $em->getRepository('WesleyBundle:Board')->find($boardId);
        $user = $this->getUser();
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('danger', 'Log in om een bericht achter te laten.');
            return $this->redirectToRoute('wesley_board_view', ['id' => $boardId, 'title' => $board->getTitle()]);
        }
        if (is_null($board)) {
            return $this->createNotFoundException('De opgevraagde aanvraag kan niet worden gevonden');
        }
        $post = new Post();
        $form = $this->createForm(AddPostType::class, $post);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $post->setBoard($board);
            $post->setUser($user);
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'Succesvol gepost.');
            return $this->redirectToRoute('wesley_board_view', ['id' => $boardId, 'title' => $board->getTitle()]);
        }

        return $this->render('@Wesley/client/add_post.html.twig', [
            'form' => $form->createView(),
            'board' => $board->getTitle(),
        ]);
    }
}
