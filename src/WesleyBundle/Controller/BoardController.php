<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BoardController extends Controller
{
    /**
     * @Route("/board/{id}/{title}/")
     *
     * @param         $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $board = $em->getRepository('WesleyBundle:Board')->find($id);
        if (is_null($board)) {
            return $this->createNotFoundException('De opgevraagde aanvraag kan niet worden gevonden');
        }
        return $this->render(
            '@Wesley/client/board.html.twig', [
            'board' => $board,
        ]);
    }

}
