<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $boards = $this->getDoctrine()->getRepository('WesleyBundle:Board')->findAll();
        $posts = $this->getDoctrine()->getRepository('WesleyBundle:Post')->findBy([], ['creationDate' => 'DESC'], 5);

        return $this->render(
            '@Wesley/client/index.html.twig', [
            'boards' => $boards,
            'posts' => $posts,
        ]);
    }
}
