<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MessageController extends Controller
{
    /**
     * @Route("/message/{id}/{title}/")
     *
     * @param         $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('WesleyBundle:Message')->find($id);
        if (is_null($message)) {
            return $this->createNotFoundException('De opgevraagde aanvraag kan niet worden gevonden');
        }
        return $this->render(
            '@Wesley/client/message.html.twig', [
            'message' => $message,
        ]);
    }

}
