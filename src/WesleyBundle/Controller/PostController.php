<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    /**
     * @Route("/post/{id}/{title}/")
     *
     * @param         $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('WesleyBundle:Post')->find($id);
        $board_title = $post->getBoard()->getTitle();
        $board_id = $post->getBoard()->getId();
        if (is_null($post)) {
            return $this->createNotFoundException('De opgevraagde aanvraag kan niet worden gevonden');
        }
        return $this->render(
            '@Wesley/client/post.html.twig', [
            'post' => $post,
            'board' => $board_title,
            'board_id' => $board_id,
        ]);
    }

}
