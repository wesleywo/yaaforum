<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WesleyBundle\Entity\User;
use WesleyBundle\Form\UserRegistrationType;

class RegistrationController extends Controller
{
    /**
     * @Route("/registreren/")
     */
    public function indexAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserRegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $userManager = $this->get('fos_user.user_manager');
            $user->setEnabled(false);
            $userManager->updateUser($user, true);

//            $content = $this->renderView(
//                '@Wesley/emails/request_created.html.twig', ['client' => $client]
//            );
//            $this->get('wesley.mailer')->send(
//                'Nieuwe Wesley accountaanvraag', 'info@wesley.nl', $content);

            $this->addFlash('success', 'Succesvol geregistreerd. Een e-mail is verzonden om Uw account te activeren.');
            return $this->redirectToRoute('wesley_default_index');
        }

        return $this->render('@Wesley/client/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
