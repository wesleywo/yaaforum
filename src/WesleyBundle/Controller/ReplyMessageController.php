<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WesleyBundle\Entity\Board;
use WesleyBundle\Entity\Message;
use WesleyBundle\Entity\Post;
use WesleyBundle\Form\AddMessageType;
use WesleyBundle\Form\AddPostType;

class ReplyMessageController extends Controller
{
    /**
     * @Route("/reply-message/{postId}/{replyId}")
     *
     * @param         $postId , $replyId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $postId, $replyId)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('WesleyBundle:Post')->find($postId);
        $thismessage = $em->getRepository('WesleyBundle:Message')->find($replyId);
        $user = $this->getUser();
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('danger', 'Log in om een bericht achter te laten.');
            return $this->redirectToRoute('wesley_post_view', ['id' => $postId, 'title' => $post->getTitle()]);
        }
        if (is_null($post)) {
            return $this->createNotFoundException('De opgevraagde aanvraag kan niet worden gevonden');
        }
        $message = new Message();
        $form = $this->createForm(AddMessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $message->setPost($post);
            $message->setUser($user);
            $message->addReply($thismessage);
            $em->persist($message);
            $em->flush();
            $this->addFlash('success', 'Succesvol gepost.');
            return $this->redirectToRoute('wesley_post_view', ['id' => $postId, 'title' => $post->getTitle()]);
        }

        return $this->render('@Wesley/client/add_message.html.twig', [
            'form' => $form->createView(),
            'post' => $post->getTitle(),
        ]);
    }
}
