<?php

namespace WesleyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WesleyBundle\Form\EditProfileType;

class UserProfileController extends Controller
{
    /**
     * @Route("/user/{id}/{username}/")
     *
     * @param         $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $profile = $em->getRepository('WesleyBundle:User')->find($id);
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('danger', 'Log in om een profiel te zien.');
            return $this->redirectToRoute('wesley_default_index');
        }
        if (is_null($profile)) {
            return $this->createNotFoundException('De opgevraagde aanvraag kan niet worden gevonden');
        }
        if ($this->getUser() == $profile) {
            $form = $this->createForm(EditProfileType::class, $profile);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em->persist($profile);
                $em->flush();
                $this->addFlash('success', 'Succesvol aangepast!.');
                return $this->redirectToRoute('wesley_userprofile_view', ['id' => $id, 'username' => $profile->getUsername()]);
            }

            return $this->render(
                '@Wesley/client/profile.html.twig', [
                'profile' => $profile,
                'form' => $form->createView(),
            ]);
        }
        return $this->render(
            '@Wesley/client/profile.html.twig', [
            'profile' => $profile,
            'form' => null,
        ]);
    }

}
