<?php
/**
 * Created by PhpStorm.
 * User: wesley
 * Date: 07-12-16
 * Time: 16:05
 */

namespace WesleyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity()
 */
class Board
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Post
     * @ORM\OneToMany(targetEntity="WesleyBundle\Entity\Post", mappedBy="board")
     */
    protected $posts;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @NotBlank()
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @NotBlank()
     */
    protected $description;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Board
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Board
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add post
     *
     * @param \WesleyBundle\Entity\Post $post
     *
     * @return Board
     */
    public function addPost(\WesleyBundle\Entity\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \WesleyBundle\Entity\Post $post
     */
    public function removePost(\WesleyBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get post
     *
     * @return \Doctrine\Common\Collections\Collection|Post[]
     */
    public function getPosts()
    {
        return $this->posts;
    }
}
