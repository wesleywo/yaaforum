<?php
/**
 * Created by PhpStorm.
 * User: wesley
 * Date: 07-12-16
 * Time: 16:05
 */

namespace WesleyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Message
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Post
     * @ORM\ManyToOne(targetEntity="WesleyBundle\Entity\Post", inversedBy="messages")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $post;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="WesleyBundle\Entity\User", inversedBy="messages")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $user;

    /**
     * @var $this
     * @ORM\ManyToOne(targetEntity="WesleyBundle\Entity\Message", inversedBy="reply")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $reply;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @NotBlank()
     */
    protected $content;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {

        $this->creationDate = new \DateTime();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set content
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Message
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Get creationDate
     *
     * @return string
     */
    public function getCreationDateTimeElapsed()
    {
        $absoluteTime = $this->creationDate->format('Y-m-d H:i:s');
        $absoluteTime = strtotime($absoluteTime);
        $cur_time = time();
        $time_elapsed = $cur_time - $absoluteTime;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        } //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        } //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        } //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        } //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        } //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        } //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    /**
     * Set post
     *
     * @param \WesleyBundle\Entity\Post $post
     *
     * @return Message
     */
    public function setPost(\WesleyBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \WesleyBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set user
     *
     * @param \WesleyBundle\Entity\User $user
     *
     * @return Message
     */
    public function setUser(\WesleyBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \WesleyBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Add reply
     *
     * @param \WesleyBundle\Entity\Message $reply
     *
     * @return Message
     */
    public function addReply(\WesleyBundle\Entity\Message $reply)
    {
        $this->reply[] = $reply;

        return $this;
    }

    /**
     * Get reply
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReply()
    {
        return $this->reply;
    }

    /**
     * Set reply
     *
     * @param \WesleyBundle\Entity\Message $reply
     *
     * @return Message
     */
    public function setReply(\WesleyBundle\Entity\Message $reply = null)
    {
        $this->reply = $reply;

        return $this;
    }
}
