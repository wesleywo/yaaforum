<?php
/**
 * Created by PhpStorm.
 * User: wesley
 * Date: 07-12-16
 * Time: 16:05
 */

namespace WesleyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Post
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Board
     * @ORM\ManyToOne(targetEntity="WesleyBundle\Entity\Board", inversedBy="posts")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $board;

    /**
     * @var Message
     * @ORM\OneToMany(targetEntity="WesleyBundle\Entity\Message", mappedBy="post")
     */
    protected $messages;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="WesleyBundle\Entity\User", inversedBy="posts")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $user;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @NotBlank()
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @NotBlank()
     */
    protected $content;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {

        $this->creationDate = new \DateTime();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Post
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Get creationDate
     *
     * @return string
     */
    public function getCreationDateTimeElapsed()
    {
        $absoluteTime = $this->creationDate->format('Y-m-d H:i:s');
        $absoluteTime = strtotime($absoluteTime);
        $cur_time = time();
        $time_elapsed = $cur_time - $absoluteTime;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        } //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        } //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        } //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        } //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        } //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        } //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    /**
     * Set board
     *
     * @param \WesleyBundle\Entity\Board $board
     *
     * @return Post
     */
    public function setBoard(\WesleyBundle\Entity\Board $board = null)
    {
        $this->board = $board;

        return $this;
    }

    /**
     * Get board
     *
     * @return \WesleyBundle\Entity\Board
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * Add message
     *
     * @param \WesleyBundle\Entity\Message $message
     *
     * @return Post
     */
    public function addMessage(\WesleyBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \WesleyBundle\Entity\Message $message
     */
    public function removeMessage(\WesleyBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set user
     *
     * @param \WesleyBundle\Entity\User $user
     *
     * @return Post
     */
    public function setUser(\WesleyBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \WesleyBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
