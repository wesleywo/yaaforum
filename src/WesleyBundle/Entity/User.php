<?php
/**
 * Copyright (c) GOScripting B.V. 2016. en Wesley Wouters 2016
 */

namespace WesleyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints\NotBlank;

//* @ORM\InheritanceType("JOINED")
//* @ORM\DiscriminatorColumn(name="type", type="string")
//* @ORM\DiscriminatorMap({"admin" = "Admin", "customer" = "Customer"})

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Post
     * @ORM\OneToMany(targetEntity="WesleyBundle\Entity\Post", mappedBy="user")
     */
    protected $posts;

    /**
     * @var Post
     * @ORM\OneToMany(targetEntity="WesleyBundle\Entity\Message", mappedBy="user")
     */
    protected $messages;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $profile_description;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $registrationDate;

    /**
     * Set registrationDate
     *
     * @param \DateTime $registrationDate
     *
     * @return User
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;

        return $this;
    }

    /**
     * Get registrationDate
     *
     * @return \DateTime
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {

        $this->registrationDate = new \DateTime();
    }

    /**
     * Add post
     *
     * @param \WesleyBundle\Entity\Post $post
     *
     * @return User
     */
    public function addPost(\WesleyBundle\Entity\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \WesleyBundle\Entity\Post $post
     */
    public function removePost(\WesleyBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Add message
     *
     * @param \WesleyBundle\Entity\Message $message
     *
     * @return User
     */
    public function addMessage(\WesleyBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \WesleyBundle\Entity\Message $message
     */
    public function removeMessage(\WesleyBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set profileDescription
     *
     * @param string $profileDescription
     *
     * @return User
     */
    public function setProfileDescription($profileDescription)
    {
        $this->profile_description = $profileDescription;

        return $this;
    }

    /**
     * Get profileDescription
     *
     * @return string
     */
    public function getProfileDescription()
    {
        return $this->profile_description;
    }
}
